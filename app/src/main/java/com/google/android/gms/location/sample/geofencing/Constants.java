/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.gms.location.sample.geofencing;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

/**
 * Constants used in this sample.
 */

final class Constants {

    private Constants() {
    }

    private static final String PACKAGE_NAME = "com.google.android.gms.location.Geofence";

    static final String GEOFENCES_ADDED_KEY = PACKAGE_NAME + ".INVP_GEOFENCE";


    //    static final float GEOFENCE_RADIUS_IN_METERS = 1609; // 1 mile, 1.6 km
    static final float GEOFENCE_RADIUS_IN_METERS = 350;

    /**
     * Map for storing information about airports in the San Francisco bay area.
     */
    static final HashMap<String, LatLng> BAY_AREA_LANDMARKS = new HashMap<>();

    static {
        BAY_AREA_LANDMARKS.put("C01,Siam Paragon,2,1", new LatLng(13.7458114, 100.5347739));
        BAY_AREA_LANDMARKS.put("C02,Siam Square One,6,1", new LatLng(13.7454075, 100.5342438));
        BAY_AREA_LANDMARKS.put("C03,Siam Center,10,1", new LatLng(13.7460735, 100.5329976));
        BAY_AREA_LANDMARKS.put("C04,Siam Discover,14,1", new LatLng(13.7464384, 100.5311697));
        BAY_AREA_LANDMARKS.put("C05,Aneee Rd.,18,1", new LatLng(13.7445614, 100.5356362));
        BAY_AREA_LANDMARKS.put("C06,Account Chula,22,1", new LatLng(13.7337471, 100.5302798));
        BAY_AREA_LANDMARKS.put("C07,VIE hotel,26,1", new LatLng(13.7506930, 100.5316882));
        BAY_AREA_LANDMARKS.put("C08,Around MJC Rat.,30,2", new LatLng(13.8285872, 100.5686559));
        BAY_AREA_LANDMARKS.put("C09,AVN,34,2",new LatLng(13.8277355,100.5676661));
        BAY_AREA_LANDMARKS.put("C10,SCB,38,2",new LatLng(13.8270684,100.567213));
        BAY_AREA_LANDMARKS.put("C11,EB,42,2",new LatLng(13.8256220,100.5675676));
        BAY_AREA_LANDMARKS.put("C12,PHY,46,2",new LatLng(13.8288447,100.5694408));
        BAY_AREA_LANDMARKS.put("C13,KU,50,2",new LatLng(13.8474782,100.5696211));
        BAY_AREA_LANDMARKS.put("C14,SPU,54,2",new LatLng(13.8557316,100.5856336));
        BAY_AREA_LANDMARKS.put("C15,UTCC,58,2",new LatLng(13.7798157,100.5603233));
        BAY_AREA_LANDMARKS.put("C16,Around MJC Rat2.,62,2",new LatLng(13.8283934,100.5682937));
        BAY_AREA_LANDMARKS.put("C17,Siam Paragon 2,66,1",new LatLng(13.7459836,100.5348114));
        BAY_AREA_LANDMARKS.put("C18,Siam Paragon 3,70,1",new LatLng(13.7460954,100.5350786));
        BAY_AREA_LANDMARKS.put("C19,Around MJC Rat3.,74,2",new LatLng(13.8287561,100.5684131));

        BAY_AREA_LANDMARKS.put("C201,Software Park,202,1",new LatLng(13.904366,100.529838));
    }
}
